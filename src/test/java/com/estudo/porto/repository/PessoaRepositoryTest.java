package com.estudo.porto.repository;

import com.estudo.porto.model.Pessoa;
import com.estudo.porto.service.PessoaService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
class PessoaRepositoryTest {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Test
    void findPessoaGetAll() {

        // Call the method under test
        List<Pessoa> result = pessoaRepository.findAll();

        // Assertions
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(45);

    }




    @Test
    public void sortPessoaJava8() {
        List<Pessoa> result = pessoaRepository.findAll();
        final List<Pessoa> greenApples = result.stream().filter(a -> a.getSexo().equals("Masculino")).collect(toList());

        assertThat(result).isNotNull();
        assertThat(greenApples.size()).isEqualTo(24);
    }

    @Test
    public void sortPessoaFilterBairros() {
        List<Pessoa> result = pessoaRepository.findAll();
        List<Pessoa> pessoasFiltrados = result.stream()
                .filter(pessoa -> {
                    return pessoa.getBairro().equals("Santa Vitória") ;
                }).collect(toList());
        assertThat(result).isNotNull();
        assertThat(pessoasFiltrados.size()).isEqualTo(1);
    }
    @Test
    public void sortPessoaFilterPeso() {
        List<Pessoa> result = pessoaRepository.findAll();
        List<Pessoa> pessoasFiltrados = result.stream()
                .filter(pessoa -> {
                    return pessoa.getPeso() >= 50;
                }).collect(toList());
        assertThat(result).isNotNull();
        assertThat(pessoasFiltrados.size()).isEqualTo(42);
    }

    @Test
    public void sortPessoaFilterCidade() {
        List<Pessoa> result = pessoaRepository.findAll();
        List<Pessoa> pessoasFiltrados = result.stream()
                .filter(pessoa -> {
                    return pessoa.getCidade().equals("São Luís");
                }).collect(toList());
        assertThat(result).isNotNull();
        assertThat(pessoasFiltrados.size()).isEqualTo(4);
    }



    @Test
    public void sortPessoaStreamMappping() {
        List<Pessoa> result = pessoaRepository.findAll();
        List<String> pessoasFiltrados = result.stream().collect(mapping(Pessoa::getCidade, toList()));;
        System.out.println(pessoasFiltrados);
        assertThat(result).isNotNull();
        assertThat(pessoasFiltrados.size()).isEqualTo(45);
    }

    @Test
    public void sortPessoaStreamVerificandoElemento() {
        List<Pessoa> result = pessoaRepository.findAll();
        List<Pessoa> pessoasFiltrados = result.stream()
                .filter(pessoa -> {
                    return pessoa.getCidade().contains("São Luís");
                }).collect(toList());

        assertThat(pessoasFiltrados.size()).isEqualTo(4);
    }

    @Test
    public void sortPessoaStreamVerificandoLikeNome() {
        List<Pessoa> result = pessoaRepository.findAlunoByNomeContainingIgnoreCase("");
        List<Pessoa> pessoasFiltrados = result.stream()
                .filter(pessoa -> {
                    return pessoa.getCidade().contains("São Luís");
                }).collect(toList());

        assertThat(pessoasFiltrados.size()).isEqualTo(4);
    }

    @Test
    public void whenCreatesEmptyOptional_thenCorrect() {
        Optional<String> empty = Optional.empty();
        assertFalse(empty.isPresent());
    }


    @Test
    public void givenNonNull_whenCreatesNonNullable_thenCorrect() {
        String name = "baeldung";
        Optional<String> opt = Optional.of(name);
        assertTrue(opt.isPresent());
    }
    @Test
    public void sortPessoaStreamVerificandoElementoContains() {
        List<Pessoa> result = pessoaRepository.findAll();
        List<Pessoa> pessoasFiltrados = result.stream()
                .filter(pessoa -> {
                    return pessoa.getCidade().contains("");
                }).collect(toList());
        assertThat(pessoasFiltrados.size()).isEqualTo(4);
    }


}










