package com.estudo.porto.model;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Entity
@Data
@Builder
@Table(name = "pessoa")
public class Pessoa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "nome", nullable = false)
    private String  nome;
    @Column(name = "idade", nullable = false)
    private Integer idade;
    @Column(name = "cpg", nullable = false)
    private String cpf;
    @Column(name = "rg", nullable = false)
    private String rg;
    @Column(name = "data_nasc", nullable = true)
    private Instant data_nasc;
    @Column(name = "sexo", nullable = false)
    private String sexo;
    @Column(name = "signo", nullable = false)
    private String signo;
    @Column(name = "mae", nullable = false)
    private String mae;
    @Column(name = "pai", nullable = false)
    private String pai;
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "senha", nullable = false)
    private String senha;
    @Column(name = "cep", nullable = false)
    private String cep;
    @Column(name = "endereco", nullable = false)
    private String endereco;
    @Column(name = "numero", nullable = false)
    private Integer numero;
    @Column(name = "bairro", nullable = false)
    private String bairro;
    @Column(name = "cidade", nullable = false)
    private String cidade;
    @Column(name = "estado", nullable = false)
    private String estado;
    @Column(name = "telefone_fixo", nullable = false)
    private String telefone_fixo;
    @Column(name = "celular", nullable = false)
    private String celular;
    @Column(name = "altura", nullable = false)
    private String altura;
    @Column(name = "peso", nullable = false)
    private Integer peso;
    @Column(name = "tipo_sanguineo", nullable = false)
    private String tipo_sanguineo;
    @Column(name = "cor", nullable = false)
    private String cor;

    public Pessoa(Integer id, String nome, Integer idade, String cpf, String rg, Instant data_nasc, String sexo, String signo, String mae, String pai, String email, String senha, String cep, String endereco, Integer numero, String bairro, String cidade, String estado, String telefone_fixo, String celular, String altura, Integer peso, String tipo_sanguineo, String cor) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
        this.rg = rg;
        this.data_nasc = data_nasc;
        this.sexo = sexo;
        this.signo = signo;
        this.mae = mae;
        this.pai = pai;
        this.email = email;
        this.senha = senha;
        this.cep = cep;
        this.endereco = endereco;
        this.numero = numero;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.telefone_fixo = telefone_fixo;
        this.celular = celular;
        this.altura = altura;
        this.peso = peso;
        this.tipo_sanguineo = tipo_sanguineo;
        this.cor = cor;
    }

    public Pessoa() {

    }
}
