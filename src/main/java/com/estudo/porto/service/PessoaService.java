package com.estudo.porto.service;


import com.estudo.porto.model.Pessoa;

import java.util.List;
import java.util.Optional;

public interface PessoaService {
    // Save operation
    Pessoa salvaPessoa(Pessoa pessoa);

    // Read operation
    List<Pessoa> buscaPessoaList();

    Optional<Pessoa> buscaPorId(Integer id);

    // Update operation
    Pessoa atualizaPessoa(Pessoa pessoa);

    // Delete operation
    void deletaPessoaById(Integer pessoaId);
}
