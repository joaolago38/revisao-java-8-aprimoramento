package com.estudo.porto.service.impl;

import com.estudo.porto.model.Pessoa;
import com.estudo.porto.repository.PessoaRepository;
import com.estudo.porto.service.PessoaService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class PessoaServiceImpl implements PessoaService {
    @Autowired
    private PessoaRepository pessoaRepository;

    @Override
    public Pessoa salvaPessoa(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    @Override
    public List<Pessoa> buscaPessoaList() {
        return (List<Pessoa>)
                pessoaRepository.findAll();
    }

    @Override
    public Optional<Pessoa> buscaPorId(Integer id) {
        return pessoaRepository.findById(id);
    }

    @Override
    public Pessoa atualizaPessoa(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    @Override
    public void deletaPessoaById(Integer pessoaId) {
        pessoaRepository.deleteById(pessoaId);
    }
}
