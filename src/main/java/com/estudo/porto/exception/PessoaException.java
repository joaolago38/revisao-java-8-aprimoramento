package com.estudo.porto.exception;

public class PessoaException extends RuntimeException {

	private static final long serialVersionUID = -7737840351898585883L;

	public PessoaException() {
    }

    public PessoaException(String message) {
        super(message);
    }

    public PessoaException(String message, Throwable cause) {
        super(message, cause);
    }

    public PessoaException(Throwable cause) {
        super(cause);
    }

    public PessoaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
