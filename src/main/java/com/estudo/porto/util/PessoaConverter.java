package com.estudo.porto.util;


import com.estudo.porto.dto.PessoaRequest;
import com.estudo.porto.dto.PessoaResponse;
import com.estudo.porto.model.Pessoa;

public class PessoaConverter {

    public static PessoaResponse toPessoaResponse(Pessoa pessoa) {

        PessoaResponse pessoaResponse = new PessoaResponse();

        pessoaResponse.setId(pessoa.getId());
        pessoaResponse.setNome(pessoa.getNome());
        pessoaResponse.setIdade(pessoa.getIdade());
        pessoaResponse.setCpf(pessoa.getCpf());
        pessoaResponse.setRg(pessoa.getRg());
        pessoaResponse.setData_nasc(pessoa.getData_nasc());
        pessoaResponse.setSexo(pessoa.getSexo());
        pessoaResponse.setSigno(pessoa.getSigno());
        pessoaResponse.setMae(pessoa.getMae());
        pessoaResponse.setPai(pessoa.getPai());
        pessoaResponse.setEmail(pessoa.getEmail());
        pessoaResponse.setSenha(pessoa.getSenha());
        pessoaResponse.setCep(pessoa.getCep());
        pessoaResponse.setEndereco(pessoa.getEndereco());
        pessoaResponse.setNumero(pessoa.getNumero());
        pessoaResponse.setBairro(pessoa.getBairro());
        pessoaResponse.setCidade(pessoa.getCidade());
        pessoaResponse.setEstado(pessoa.getEstado());
        pessoaResponse.setCidade(pessoa.getCidade());
        pessoaResponse.setTelefone_fixo(pessoa.getTelefone_fixo());
        pessoaResponse.setCelular(pessoa.getCelular());
        pessoaResponse.setAltura(pessoa.getAltura());
        pessoaResponse.setPeso(pessoa.getPeso());
        pessoaResponse.setTipo_sanguineo(pessoa.getTipo_sanguineo());
        pessoaResponse.setCor(pessoa.getCor());
        return pessoaResponse;
    }

    public static PessoaRequest toPessoaRequest(Pessoa pessoa) {
        PessoaRequest pessoaRequest = new PessoaRequest();
        pessoaRequest.setId(pessoa.getId());
        pessoaRequest.setNome(pessoa.getNome());
        pessoaRequest.setIdade(pessoa.getIdade());
        pessoaRequest.setCpf(pessoa.getCpf());
        pessoaRequest.setRg(pessoa.getRg());
        pessoaRequest.setData_nasc(pessoa.getData_nasc());
        pessoaRequest.setSexo(pessoa.getSexo());
        pessoaRequest.setSigno(pessoa.getSigno());
        pessoaRequest.setMae(pessoa.getMae());
        pessoaRequest.setPai(pessoa.getPai());
        pessoaRequest.setEmail(pessoa.getEmail());
        pessoaRequest.setSenha(pessoa.getSenha());
        pessoaRequest.setCep(pessoa.getCep());
        pessoaRequest.setEndereco(pessoa.getEndereco());
        pessoaRequest.setNumero(pessoa.getNumero());
        pessoaRequest.setBairro(pessoa.getBairro());
        pessoaRequest.setCidade(pessoa.getCidade());
        pessoaRequest.setEstado(pessoa.getEstado());
        pessoaRequest.setCidade(pessoa.getCidade());
        pessoaRequest.setTelefone_fixo(pessoa.getTelefone_fixo());
        pessoaRequest.setCelular(pessoa.getCelular());
        pessoaRequest.setAltura(pessoa.getAltura());
        pessoaRequest.setPeso(pessoa.getPeso());
        pessoaRequest.setTipo_sanguineo(pessoa.getTipo_sanguineo());
        pessoaRequest.setCor(pessoa.getCor());


        return pessoaRequest;
    }

    public static Pessoa toPessoa(PessoaRequest pessoaRequest) {
        Pessoa pessoa = new Pessoa();

        pessoa.setId(pessoa.getId());
        pessoa.setNome(pessoa.getNome());
        pessoa.setIdade(pessoa.getIdade());
        pessoa.setCpf(pessoa.getCpf());
        pessoa.setRg(pessoa.getRg());
        pessoa.setData_nasc(pessoa.getData_nasc());
        pessoa.setSexo(pessoa.getSexo());
        pessoa.setSigno(pessoa.getSigno());
        pessoa.setMae(pessoa.getMae());
        pessoa.setPai(pessoa.getPai());
        pessoa.setEmail(pessoa.getEmail());
        pessoa.setSenha(pessoa.getSenha());
        pessoa.setCep(pessoa.getCep());
        pessoa.setEndereco(pessoa.getEndereco());
        pessoa.setNumero(pessoa.getNumero());
        pessoa.setBairro(pessoa.getBairro());
        pessoa.setCidade(pessoa.getCidade());
        pessoa.setEstado(pessoa.getEstado());
        pessoa.setCidade(pessoa.getCidade());
        pessoa.setTelefone_fixo(pessoa.getTelefone_fixo());
        pessoa.setCelular(pessoa.getCelular());
        pessoa.setAltura(pessoa.getAltura());
        pessoa.setPeso(pessoa.getPeso());
        pessoa.setTipo_sanguineo(pessoa.getTipo_sanguineo());
        pessoa.setCor(pessoa.getCor());
        return pessoa;
    }

}
