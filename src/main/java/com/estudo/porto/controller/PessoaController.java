package com.estudo.porto.controller;



import com.estudo.porto.model.Pessoa;
import com.estudo.porto.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PessoaController {
    @Autowired private PessoaService pessoaService;

    // Save operation
    @PostMapping("/pessoa")
    public Pessoa saveDepartment(@Valid @RequestBody Pessoa pessoa)
    {
        return pessoaService.salvaPessoa(pessoa);
    }

    // Read operation
    @GetMapping("/pessoa")
    public List<Pessoa> buscaPessoaList()
    {
        return pessoaService.buscaPessoaList();
    }

    // Update operation
    @PutMapping("/pessoa")
    public Pessoa
    updateDepartment(@RequestBody Pessoa pessoa)
    {
        return pessoaService.atualizaPessoa(pessoa);
    }

    // Delete operation
    @DeleteMapping("/pessoa/{id}")
    public String deleteDepartmentById(@PathVariable("id")
                                       Integer Id)
    {
        pessoaService.deletaPessoaById(
                Id);
        return "Apagado com Sucesso";
    }
}
