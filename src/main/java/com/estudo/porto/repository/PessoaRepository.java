package com.estudo.porto.repository;


import com.estudo.porto.model.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {
    List<Pessoa> findAlunoByNomeContainingIgnoreCase(String nome);
}
