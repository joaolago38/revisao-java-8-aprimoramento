package com.estudo.porto.dto;

import com.estudo.porto.util.DataDeserializer;
import com.estudo.porto.util.DataSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

import java.time.Instant;
@Data
@JsonPropertyOrder({"nome","idade","cpf","rg","data_nasc","sexo","signo","mae","pai",
        "email","senha","cep","endereco","numero", "bairro","cidade","estado","telefone_fixo",
        "celular","altura","peso","tipo_sanguineo","cor"})
public class PessoaRequest {

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private Integer id;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("nome")
    private String  nome;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("idade")
    private Integer idade;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cpf")
    private String cpf;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("rg")
    private String rg;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonDeserialize(using = DataDeserializer.class)
    @JsonSerialize(using = DataSerializer.class)
    @JsonProperty("data_nasc")
    private Instant data_nasc;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("sexo")
    private String sexo;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("signo")
    private String signo;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("mae")
    private String mae;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("pai")
    private String pai;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("email")
    private String email;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("senha")
    private String senha;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cep")
    private String cep;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("endereco")
    private String endereco;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("numero")
    private Integer numero;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("bairro")
    private String bairro;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cidade")
    private String cidade;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("estado")
    private String estado;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("telefone_fixo")
    private String telefone_fixo;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("celular")
    private String celular;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("altura")
    private String altura;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("peso")
    private Integer peso;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("tipo_sanguineo")
    private String tipo_sanguineo;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cor")
    private String cor;
}
