package com.estudo.porto.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "nome","idade","cpf","rg","data_nasc","sexo","signo","mae","pai",
        "email","senha","cep","endereco","numero", "bairro","cidade","estado","telefone_fixo",
        "celular","altura","peso","tipo_sanguineo","cor"})
public class PessoaResponse extends PessoaRequest {

}
