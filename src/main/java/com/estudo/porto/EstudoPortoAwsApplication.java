package com.estudo.porto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EstudoPortoAwsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EstudoPortoAwsApplication.class, args);
	}

}
